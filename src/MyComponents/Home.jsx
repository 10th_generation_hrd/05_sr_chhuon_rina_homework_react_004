import React from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";

export default function Home(data) {
  return (
    <Container style={{height:"800px"}}>
      <h1 style={{ textAlign: "left" }}>Trending Course</h1>
      <Row>
        {data.data.map((item)=>(
          <Col>
          <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={item.thumbnail} />
            <Card.Body>
              <Card.Title >{item.title}</Card.Title>
              <Card.Text>
                {item.description}
              </Card.Text>
              <h4>{item.price}</h4>
              <Button variant="success">Buy the course</Button>
            </Card.Body>
          </Card>
        </Col>
        ))

        }
        
      </Row>
    </Container>
  );
}
