import React from "react";
import { Container, Navbar, Nav} from "react-bootstrap";
import { NavLink } from "react-router-dom";

export default function NavBar() {
  return (
    <div>
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand href="#home">PT-004</Navbar.Brand>
          <Nav className="justify-content-end">
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/aboutus">AboutUs</Nav.Link>
            <Nav.Link as={NavLink} to="/ourvision">OurVision</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
}
