import React from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'

export default function AboutUs() {
  return (
    <Container style={{height: "800px"}}>
        <Row>
            <Col>
                <Image src="https://kshrd.com.kh/static/media/1.6169e38f.jpg" alt="" style={{width: 640, height:200}}/>
            </Col>
            <Col style={{textAlign: "left"}}>
                <p>
                Korea Software HRD Center is an academy training center for training software professionals in cooperation with Korea International Cooperation Agency(KOICA) and Webcash in April, 2013 in Phnom Penh, Cambodia.
                </p>
                <p>From 2020, Korea Software HRD Center has been become Global NGO with the name Foundation for Korea Software Global Aid (KSGA), main sponsored by Webcash Group, to continue mission for ICT Development in Cambodia and will recruit 60 t0 80 scholarship students every year.</p>
            </Col>
        </Row>
        <Row>
            
            <Col style={{textAlign: "left"}}>
                <p>
                Korea Software HRD Center is an academy training center for training software professionals in cooperation with Korea International Cooperation Agency(KOICA) and Webcash in April, 2013 in Phnom Penh, Cambodia.
                </p>
                <p>From 2020, Korea Software HRD Center has been become Global NGO with the name Foundation for Korea Software Global Aid (KSGA), main sponsored by Webcash Group, to continue mission for ICT Development in Cambodia and will recruit 60 t0 80 scholarship students every year.</p>
            </Col>
            <Col>
                <Image src="https://kshrd.com.kh/static/media/1.6169e38f.jpg" alt="" style={{width: 640, height:200}}/>
            </Col>
        </Row>
        <Row>
            <Col>
                <Image src="https://kshrd.com.kh/static/media/1.6169e38f.jpg" alt="" style={{width: 640, height:200}}/>
            </Col>
            <Col style={{textAlign: "left"}}>
                <p>
                Korea Software HRD Center is an academy training center for training software professionals in cooperation with Korea International Cooperation Agency(KOICA) and Webcash in April, 2013 in Phnom Penh, Cambodia.
                </p>
                <p>From 2020, Korea Software HRD Center has been become Global NGO with the name Foundation for Korea Software Global Aid (KSGA), main sponsored by Webcash Group, to continue mission for ICT Development in Cambodia and will recruit 60 t0 80 scholarship students every year.</p>
            </Col>
        </Row>
    </Container>
  )
}
