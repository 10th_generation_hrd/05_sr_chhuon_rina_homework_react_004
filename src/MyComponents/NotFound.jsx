import React from 'react'
import { Container, Image } from 'react-bootstrap'

export default function NotFound() {
  return (
    <Container style={{height: "800px"}}>
        {/* <p>Page not found</p> */}
        <Image src="https://www.prestashop.com/sites/default/files/wysiwyg/404_not_found.png"></Image>
    </Container>
  )
}
