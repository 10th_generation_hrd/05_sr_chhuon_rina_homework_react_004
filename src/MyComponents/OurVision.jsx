import React from "react";
import { Col, Container, Row } from "react-bootstrap";

export default function () {
  return (
    <Container style={{ height: "800px" }}>
      <Row>
        <Col style={{ backgroudColor: "#f5f5f5" }} className="m-2">
          <div>
            <h3>Vision</h3>
            <ul style={{ textAlign: "left" }}>
              <li>
                {" "}
                To be the best SW Professional Training Center in Cambodia
              </li>
            </ul>
          </div>
        </Col>
        <Col style={{ backgroudColor: "#f5f5f5" }} className="m-2">
          <div>
            <h3>Mission</h3>
            <ul style={{ textAlign: "left" }}>
              <li>High quality training and research</li>
              <li> Developing Capacity of SW Experts to be Leaders in IT </li>
              <li> Developing sustainable ICT Program</li>
            </ul>
          </div>
        </Col>
      </Row>
      <Row>
        <Col style={{ backgroudColor: "#f5f5f5" }} className="m-2">
          <div>
            <h3>Strategy</h3>
            <ul style={{ textAlign: "left" }}>
              <li>
                {" "}
                Best training method with up to date curriculum and environment
              </li>
              <li>
                Cooperation with the best IT industry to guarantee student's
                career and benefit
              </li>
              <li>Additional Soft Skill, Management, Leadership training</li>
            </ul>
          </div>
        </Col>
        <Col style={{ backgroudColor: "#f5f5f5" }} className="m-2">
          <h3>Slogan</h3>
          <ul style={{ textAlign: "left" }}>
            <li>"KSHRD, connects you to various opportunities in IT Field."</li>
            <li>
              Raising brand awareness with continuous advertisement of SNS and
              any other media
            </li>
          </ul>
        </Col>
      </Row>
    </Container>
  );
}
