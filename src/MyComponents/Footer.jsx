import React from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'

export default function Footer() {
  return (
    <Container>
        <Row>
            <Col>
                <Image src="https://kshrd.com.kh/static/media/logo.f368c431.png" style={{width: 70, heigth: 80}}></Image>
                <p><strong> <sup>&#169;</sup>  រក្សាសិទ្ធគ្រប់យ៉ាងដោយ</strong> KSHRD Center <strong>ឆ្នាំ២០២២</strong> </p>
                
            </Col>
            <Col>
                <h5>Address</h5>
                <small><strong>Address:</strong> #12, St323, Sangket Boeung Kak II, Khan Toul Louk, Phnom Penh, Cambodia.</small>
            </Col>
            <Col>
                <h5>Contact</h5>
                <small><strong>Tel:</strong> 012 998 919(Khmer)</small>
                <br />
                <small><strong>Tel:</strong> 085 402 605(Korean)</small>
                <br />
                <small><strong>Email:</strong> info.kshrd@gamil.com phirum.gm@gamil.com</small>
            </Col>
        </Row>
    </Container>
  )
}
